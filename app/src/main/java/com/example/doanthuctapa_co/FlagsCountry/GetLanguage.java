package com.example.doanthuctapa_co.FlagsCountry;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetLanguage {
    private static FlagsCountryItem flagsCountryItem;
    public static String getKey(Context activity,String key)
    {
        String answer = "";
        flagsCountryItem = new FlagsCountryItem(activity);
        try{
            JSONObject jsonObject = new JSONObject(flagsCountryItem.getLANGUAGE());
            answer = jsonObject.getString(key);
        } catch (JSONException e){
            e.printStackTrace();
            Log.d("asd",e.toString());
        }
        return answer;
    }
}
