package com.example.doanthuctapa_co.FlagsCountry;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;

public class FlagsCountryItem {
    static String response;
    public static String getResponse()
    {
        return response;
    }
    public static void setResponse(String response){
        FlagsCountryItem.response = response;
    }
    private SharedPreferences app_prefs;
    private Context context;
    private final String FLAGSCOUNTRYITEM = "flagscountryitem",NAME = "name",LANGUAGE = "language",SELECTLANGUAGE = "selectlanguage",SOUND = "sound",RADIO = "radio";
    public FlagsCountryItem(Context context) {
        app_prefs = context.getSharedPreferences("flagscountry",
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public  void putFlagsCountryItem(String string) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(FLAGSCOUNTRYITEM, changeJSON(string).replace('=',':'));
        edit.commit();
    }
    public String getFlagsCountryItem() {
        return app_prefs.getString(FLAGSCOUNTRYITEM, "");
    }

    public static String changeJSON(String string)
    {
        Log.d("asdasd",string);
        String string1 = "";
        ArrayList<Character>  arrayList = new ArrayList<>();
        if(string != "" && !string.isEmpty())
        {
            char[] chars = string.toCharArray();
            for(int i = 0; i < chars.length - 1;i++)
            {
                arrayList.add(chars[i]);
                if(chars[i] == '{' || chars[i+1] == '=' || ( chars[i] == '[' && chars[i+1] != ']' ) || ( chars[i+1] == ',' && chars[i] != ']' ) || (chars[i-1] == ',' && chars[i] == ' ' )
                || ( chars[i+1] == ']' && chars[i] != '[' ) || ( chars[i] == '=' && chars[i+1] != '[' ) || ( chars[i] != ']' && chars[i+1] == '}' )){
                    arrayList.add('"');
                }
            }
            arrayList.add(chars[chars.length - 1]);
            for (int i = 0; i < arrayList.size(); i++)
            {
                string1 = string1 + arrayList.get(i);
            }
        }
        Log.d("asdasd",string1);
        return string1.replace('=',':');
    }
    public void putName(String name)
    {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(NAME,name);
        editor.commit();
    }
    public String getName()
    {
        return app_prefs.getString(NAME,"");
    }
    public void putLanguage(String language)
    {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(LANGUAGE,changeJSON(language));
        editor.commit();
        Log.d("asdasd",getLANGUAGE());
    }
    public String getLANGUAGE()
    {
        return app_prefs.getString(LANGUAGE,"");
    }
    public void putSELECTLANGUAGE(String select)
    {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(SELECTLANGUAGE,select);
        editor.commit();
    }
    public String getSELECTLANGUAGE()
    {
        return app_prefs.getString(SELECTLANGUAGE,"");
    }
    public void putSOUND(String sound)
    {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(SOUND,sound);
        editor.commit();
    }
    public String getSOUND()
    {
        return app_prefs.getString(SOUND,"");
    }
    public void putRadioButton(String radio)
    {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(RADIO,radio);
        editor.commit();
    }
    public String getRadioButton()
    {
        return app_prefs.getString(RADIO,"");
    }
}
