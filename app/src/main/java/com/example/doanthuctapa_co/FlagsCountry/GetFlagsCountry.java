package com.example.doanthuctapa_co.FlagsCountry;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetFlagsCountry {
    private static FlagsCountryItem flagsCountryItem;
    public static ArrayList<String> getEasyAbbreviationsNameCountry(Context activity)
    {
        ArrayList<String> arrayList = new ArrayList<>();
        flagsCountryItem = new FlagsCountryItem(activity);
        try{
            JSONObject jsonObject = new JSONObject(flagsCountryItem.getFlagsCountryItem());
            JSONArray jsonArray = jsonObject.getJSONArray("easyAbbreviationsNameCountry");
            for(int i = 0; i < jsonArray.length();i++){
                arrayList.add(jsonArray.getString(i));

            }
        } catch (JSONException e){
            e.printStackTrace();
            Log.d("asd",e.toString());
        }
        return arrayList;
    }
    public static ArrayList<String> getNormAlabbreviationsNameCountry(Context activity)
    {
        ArrayList<String> arrayList = new ArrayList<>();
        flagsCountryItem = new FlagsCountryItem(activity);
        try{
            JSONObject jsonObject = new JSONObject(flagsCountryItem.getFlagsCountryItem());
            JSONArray jsonArray = jsonObject.getJSONArray("normAlabbreviationsNameCountry");
            for(int i = 0; i < jsonArray.length();i++){
                arrayList.add(jsonArray.getString(i));

            }
        } catch (JSONException e){
            e.printStackTrace();
            Log.d("asd",e.toString());
        }
        return arrayList;
    }
    public static ArrayList<String> getHardAbbreviationsNameCountry(Context activity)
    {
        ArrayList<String> arrayList = new ArrayList<>();
        flagsCountryItem = new FlagsCountryItem(activity);
        try{
            JSONObject jsonObject = new JSONObject(flagsCountryItem.getFlagsCountryItem());
            JSONArray jsonArray = jsonObject.getJSONArray("hardAbbreviationsNameCountry");
            for(int i = 0; i < jsonArray.length();i++){
                arrayList.add(jsonArray.getString(i));

            }
        } catch (JSONException e){
            e.printStackTrace();
            Log.d("asd",e.toString());
        }
        return arrayList;
    }
    public static ArrayList<String> getEasyFullNameCountry(Context activity)
    {
        ArrayList<String> arrayList = new ArrayList<>();
        flagsCountryItem = new FlagsCountryItem(activity);
        try{
            JSONObject jsonObject = new JSONObject(flagsCountryItem.getFlagsCountryItem());
            JSONArray jsonArray = jsonObject.getJSONArray("easyFullNameCountry");
            for(int i = 0; i < jsonArray.length();i++){
                arrayList.add(jsonArray.getString(i));

            }
        } catch (JSONException e){
            e.printStackTrace();
            Log.d("asd",e.toString());
        }
        return arrayList;
    }
    public static ArrayList<String> getNormalFullNameCountry(Context activity)
    {
        ArrayList<String> arrayList = new ArrayList<>();
        flagsCountryItem = new FlagsCountryItem(activity);
        try{
            JSONObject jsonObject = new JSONObject(flagsCountryItem.getFlagsCountryItem());
            JSONArray jsonArray = jsonObject.getJSONArray("normalFullNameCountry");
            for(int i = 0; i < jsonArray.length();i++){
                arrayList.add(jsonArray.getString(i));

            }
        } catch (JSONException e){
            e.printStackTrace();
            Log.d("asd",e.toString());
        }
        return arrayList;
    }
    public static ArrayList<String> getHardFullNameCountry(Context activity)
    {
        ArrayList<String> arrayList = new ArrayList<>();
        flagsCountryItem = new FlagsCountryItem(activity);
        try{
            JSONObject jsonObject = new JSONObject(flagsCountryItem.getFlagsCountryItem());
            JSONArray jsonArray = jsonObject.getJSONArray("hardFullNameCountry");
            for(int i = 0; i < jsonArray.length();i++){
                arrayList.add(jsonArray.getString(i));

            }
        } catch (JSONException e){
            e.printStackTrace();
            Log.d("asd",e.toString());
        }
        return arrayList;
    }
}
