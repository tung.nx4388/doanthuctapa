package com.example.doanthuctapa_co.DialogFragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.doanthuctapa_co.Activity.HomeActivity;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.R;

import java.util.Random;

public class DialogFragmentWriteName extends DialogFragment implements View.OnClickListener{

    public interface GetName{
        void name(String name);
    }

    GetName getName;

    @Override
    public void onAttach(@NonNull Context context) {
        try {
            getName = (GetName) context;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
        super.onAttach(context);
    }

    EditText edtName;
    Button btOK,btRandom;
    public TextView txtWriteName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialogfragment_writename,container,false);

        mapping(v);
        intall();

        return v;
    }

    private void mapping(View v)
    {
        edtName = v.findViewById(R.id.edtName);
        btOK = v.findViewById(R.id.btFinalWriteName);
        btRandom = v.findViewById(R.id.btRandom);
        txtWriteName = v.findViewById(R.id.txtWriteName);

        changeLanguage();
    }

    private void intall()
    {
        btOK.setOnClickListener(this);
        btRandom.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btFinalWriteName:{
                HomeActivity.startSoundBT(getContext());
                if(edtName.getText().toString().compareTo("") == 0)
                {
                    Toast.makeText(getActivity(), GetLanguage.getKey(getActivity(),"txtnotwrong"), Toast.LENGTH_SHORT).show();
                }
                else {
                    getName.name(edtName.getText().toString());
                    dismiss();
                }
            }break;
            case R.id.btRandom:{
                HomeActivity.startSoundBT(getContext());
                String name = "#ID";
                Character[] characters = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
                for(int i=0; i<6;i++)
                {
                    name = name + characters[new Random().nextInt(15)];
                }
                getName.name(name);
                dismiss();
            }break;
        }
    }
    private void changeLanguage()
    {
        if (GetLanguage.getKey(getActivity(),"edtwritename").compareTo("") != 0)
        {
            txtWriteName.setText(GetLanguage.getKey(getActivity(),"edtwritename"));
        }

    }
}
