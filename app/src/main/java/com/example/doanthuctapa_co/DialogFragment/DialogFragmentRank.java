package com.example.doanthuctapa_co.DialogFragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.doanthuctapa_co.Activity.HomeActivity;
import com.example.doanthuctapa_co.Activity.PlayActivity;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.R;

public class DialogFragmentRank extends DialogFragment {

    public interface getRank{
        void ranK(boolean b);
    }

    getRank getRank;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            getRank  = (getRank) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    TextView txtFinal;
    Button btFinal;

    String name,rank,score,mod;

    public DialogFragmentRank(String name, String rank, String score, String mod) {
        this.name = name;
        this.rank = rank;
        this.score = score;
        this.mod = mod;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_finsish,container,false);
        getDialog().setCanceledOnTouchOutside(false);
        mapping(v);
        btFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.startSoundBT(getContext());
                dismiss();
                getRank.ranK(true);
            }
        });

        return v;
    }

    private void mapping(View v)
    {
        txtFinal = v.findViewById(R.id.txtFinal);
        btFinal = v.findViewById(R.id.btFinal);
        btFinal.setText(GetLanguage.getKey(getActivity(),"txtend"));

        if(GetLanguage.getKey(getActivity(),"language").compareTo("vietnam") == 0)
        {
            txtFinal.setText("Chúc mừng "+name+" đạt hạng "+rank+" ở chế độ "+mod+" với số điểm là "+score);
        }
        else {
            txtFinal.setText("Congratulations to "+name+" on reaching "+rank+" in "+mod+" mode with a score of "+score);
        }
    }
}
