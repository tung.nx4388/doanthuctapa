package com.example.doanthuctapa_co.DialogFragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.doanthuctapa_co.Activity.HomeActivity;
import com.example.doanthuctapa_co.Activity.PlayActivity;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.R;

public class DialogFragmentFinish extends DialogFragment {

    public interface getFinal{
        void finall(boolean b);
    }

    getFinal getFinal;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            getFinal  = (getFinal) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    TextView txtFinal;
    Button btFinal;
    int locationQuetion;
    String lv;
    public DialogFragmentFinish(int locationQuetion, String lv)
    {
        this.locationQuetion = locationQuetion;
        this.lv = lv;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_finsish,container,false);
        getDialog().setCanceledOnTouchOutside(false);
        mapping(v);
        btFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.startSoundBT(getContext());
                dismiss();
                getFinal.finall(true);
            }
        });

        return v;
    }

    private void mapping(View v)
    {
        txtFinal = v.findViewById(R.id.txtFinal);
        btFinal = v.findViewById(R.id.btFinal);
        btFinal.setText(GetLanguage.getKey(getActivity(),"txtend"));

        if(GetLanguage.getKey(getActivity(),"language").compareTo("vietnam") == 0)
        {
            txtFinal.setText("Chúc mừng "+ HomeActivity.name+" đã ghi được " +locationQuetion+" điểm ở mức độ "+ lv);
        }
        else {
            txtFinal.setText("Congratulations "+HomeActivity.name+" has scored " +locationQuetion+" points in "+PlayActivity.chosenLeve+" mode");
        }
    }
}
