package com.example.doanthuctapa_co.DialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.doanthuctapa_co.Activity.HomeActivity;
import com.example.doanthuctapa_co.Fragment.FlagsQuestionFragment1;
import com.example.doanthuctapa_co.Fragment.FlagsQuestionFragment2;
import com.example.doanthuctapa_co.R;

public class DialogFragmentChosenAns extends DialogFragment implements View.OnClickListener{
    Button btYes,btNo;
    FlagsQuestionFragment1 flagsQuestionFragment1;
    FlagsQuestionFragment2 flagsQuestionFragment2;
    int loca;

    public DialogFragmentChosenAns(FlagsQuestionFragment1 flagsQuestionFragment1, int loca)
    {
        this.flagsQuestionFragment1 = flagsQuestionFragment1;
        this.loca = loca;
    }

    public DialogFragmentChosenAns(FlagsQuestionFragment2 flagsQuestionFragment2, int loca)
    {
        this.flagsQuestionFragment2 = flagsQuestionFragment2;
        this.loca = loca;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_chosen_anser, container, false);

        mapping(v);

        return v;
    }
    private void mapping(View v)
    {
        btYes = v.findViewById(R.id.btYes);
        btNo = v.findViewById(R.id.btNo);

        btYes.setOnClickListener(this);
        btNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btYes:{
                HomeActivity.startSoundBT(getContext());
                if(HomeActivity.strMod.compareTo("Chế độ 1") == 0) {
//                    flagsQuestionFragment1.finalQuestion(loca);
                }
                else {
//                    flagsQuestionFragment2.finalQuestion(loca);
                }
                dismiss();
            }break;
            case R.id.btNo:{
                HomeActivity.startSoundBT(getContext());
                dismiss();
            }break;
        }
    }
}
