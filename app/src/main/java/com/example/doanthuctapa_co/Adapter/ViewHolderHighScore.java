package com.example.doanthuctapa_co.Adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doanthuctapa_co.R;

public class ViewHolderHighScore extends RecyclerView.ViewHolder {
    TextView txtRank,txtName,txtScore;

    public ViewHolderHighScore(@NonNull View itemView) {
        super(itemView);

        txtName = itemView.findViewById(R.id.txtName);
        txtRank = itemView.findViewById(R.id.txtRank);
        txtScore = itemView.findViewById(R.id.txtScore);
    }
}
