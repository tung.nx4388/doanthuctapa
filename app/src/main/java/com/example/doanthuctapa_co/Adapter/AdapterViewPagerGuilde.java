package com.example.doanthuctapa_co.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.doanthuctapa_co.R;

import java.util.ArrayList;

public class AdapterViewPagerGuilde extends PagerAdapter {
    ArrayList<ItemResourceImage> itemResourceImages;
    Context mContext;

    public AdapterViewPagerGuilde(Context mContext,ArrayList<ItemResourceImage> itemResourceImages) {
        this.mContext = mContext;
        this.itemResourceImages = itemResourceImages;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_viewpaperguilde,container,false);
        ImageView img1 = v.findViewById(R.id.img1);
        ImageView img2 = v.findViewById(R.id.img2);
        img1.setImageResource(itemResourceImages.get(position).img1);
        img2.setImageResource(itemResourceImages.get(position).img2);
        container.addView(v);
        return v;
    }

    @Override
    public int getCount() {
        return itemResourceImages.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        super.destroyItem(container, position, object);
        ((ViewPager) container).removeView((View) object);
    }
}
