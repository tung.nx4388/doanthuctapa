package com.example.doanthuctapa_co.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doanthuctapa_co.Highscore.HighscoreItem;
import com.example.doanthuctapa_co.R;

import java.util.ArrayList;

public class AdapterRecycleListHighScore extends RecyclerView.Adapter<ViewHolderHighScore> {
    ArrayList<HighscoreItem> arrayList = new ArrayList<>();

    public AdapterRecycleListHighScore(ArrayList<HighscoreItem> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolderHighScore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_highscore,parent,false);
        return new ViewHolderHighScore(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderHighScore holder, int position) {
        HighscoreItem itemHighscore = arrayList.get(position);
        holder.txtScore.setText(itemHighscore.getScore());
        holder.txtRank.setText(itemHighscore.getRank());
        holder.txtName.setText(itemHighscore.getName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
