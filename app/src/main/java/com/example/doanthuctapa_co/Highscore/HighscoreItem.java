package com.example.doanthuctapa_co.Highscore;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HighscoreItem {
    String name,rank,score;

    public HighscoreItem(String rank, String name, String score) {
        this.name = name;
        this.rank = rank;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public static ArrayList<HighscoreItem> getArrayListHighscore(String string)
    {
        ArrayList<HighscoreItem> arrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(string);
            JSONArray nameArray = jsonObject.getJSONArray("name");
            JSONArray rankArray = jsonObject.getJSONArray("rank");
            JSONArray scoreArray = jsonObject.getJSONArray("score");
            for(int i = 0; i < nameArray.length(); i++)
            {
                arrayList.add(new HighscoreItem(rankArray.getString(i),nameArray.getString(i),scoreArray.getString(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("asdad",e.toString());
        }
        return arrayList;
    }
}
