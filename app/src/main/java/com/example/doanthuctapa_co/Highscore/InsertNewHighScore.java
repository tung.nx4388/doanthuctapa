package com.example.doanthuctapa_co.Highscore;

import android.util.Log;

import com.squareup.okhttp.internal.Internal;

import java.util.ArrayList;

public class InsertNewHighScore {
    public static ArrayList<HighscoreItem> highscoreItemArrayList;

    public InsertNewHighScore(ArrayList<HighscoreItem> highscoreItemArrayList) {
        InsertNewHighScore.highscoreItemArrayList = highscoreItemArrayList;
    }

    public static ArrayList<HighscoreItem> sortHighScoreItemArrayList(ArrayList<HighscoreItem> highscoreItemArrayList)
    {
        ArrayList<HighscoreItem> arrayList = new ArrayList<>();

        int rank = 0;

        while (highscoreItemArrayList.size() > 0)
        {
            rank ++;
            int max = Integer.parseInt(highscoreItemArrayList.get(0).score);
            int choose = 0;
            for (int i = 0; i < highscoreItemArrayList.size(); i++)
            {
                if(max < Integer.parseInt(highscoreItemArrayList.get(i).score))
                {
                    max = Integer.parseInt(highscoreItemArrayList.get(i).score);
                    choose = i;
                }
            }

            if(arrayList.size() > 0) {
                if (max == Integer.parseInt(arrayList.get(arrayList.size()-1).score)) {
                    arrayList.add(new HighscoreItem(arrayList.get(arrayList.size()-1).rank, highscoreItemArrayList.get(choose).getName(), highscoreItemArrayList.get(choose).getScore()));
                } else {
                    arrayList.add(new HighscoreItem("#" + rank, highscoreItemArrayList.get(choose).getName(), highscoreItemArrayList.get(choose).getScore()));
                }
            }
            else {
                arrayList.add(new HighscoreItem("#"+rank,highscoreItemArrayList.get(choose).getName(),highscoreItemArrayList.get(choose).getScore()));
            }
            highscoreItemArrayList.remove(choose);
            if(Integer.parseInt(arrayList.get(arrayList.size()-1).getRank().replace('#','0'))>20){
                arrayList.remove(arrayList.size()-1);
                break;
            }
        }

        highscoreItemArrayList.clear();

        return arrayList;
    }
    public static HighscoreItem findRank(ArrayList<HighscoreItem> arrayList,String name, String score)
    {
        int j = 0;
        for (int i = 0; i < arrayList.size(); i++)
        {
            if(name.compareTo(arrayList.get(i).getName()) == 0)
            {
                if(score.compareTo(""+arrayList.get(i).getScore()) == 0)
                {
                    j = i;
                }
            }
        }
        return arrayList.get(j);
    }
}
