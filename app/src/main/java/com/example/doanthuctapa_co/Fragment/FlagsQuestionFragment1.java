package com.example.doanthuctapa_co.Fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.doanthuctapa_co.API.getIMAGE;
import com.example.doanthuctapa_co.Activity.HomeActivity;
import com.example.doanthuctapa_co.Activity.PlayActivity;
import com.example.doanthuctapa_co.DialogFragment.DialogFragmentChosenAns;
import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.R;

import java.util.ArrayList;

import static com.example.doanthuctapa_co.Activity.PlayActivity.arrayList4Questions;
import static com.example.doanthuctapa_co.Activity.PlayActivity.arrayListAnyQuestions;

public class FlagsQuestionFragment1 extends Fragment implements View.OnClickListener{

    MediaPlayer mediaTrue,mediaFalse;
    FlagsCountryItem flagsCountryItem;

    public interface getBoolenQuestion{
        void boolenquestion(boolean b);
    }

    getBoolenQuestion getBoolenQuestion;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
           getBoolenQuestion  = (getBoolenQuestion) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    int locaTionQuestion;

    Handler handlerQuest;
    Runnable rTrue, rFalse;
    int time = 5;
    int loca;
    int True;

    ImageView imgFlags;
    Button[] btAs = new Button[4];

    DialogFragmentChosenAns dialogFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentflagsquestion1,container,false);

        flagsCountryItem = new FlagsCountryItem(getActivity());
        mapping(view);

        arrayListAnyQuestions = PlayActivity.randomArrayListAnyQuestion();
        initialization();

        Log.d("asdasd",PlayActivity.arrayListAbbreviationsNameCountry.toString());
        Log.d("asdasd",PlayActivity.arrayListFullNameCountry.toString());

        return view;
    }

    private void mapping(View v)
    {
        arrayListAnyQuestions = new ArrayList<>();
        arrayList4Questions = new ArrayList<>();
        locaTionQuestion = 0;
        loca = -1;

        imgFlags = v.findViewById(R.id.imgFlag);
        btAs[0] = v.findViewById(R.id.btAnswer1);
        btAs[1] = v.findViewById(R.id.btAnswer2);
        btAs[2] = v.findViewById(R.id.btAnswer3);
        btAs[3] = v.findViewById(R.id.btAnswer4);

        btAs[0].setOnClickListener(this);
        btAs[1].setOnClickListener(this);
        btAs[2].setOnClickListener(this);
        btAs[3].setOnClickListener(this);
        PlayActivity.activity.btNext.setOnClickListener(this);
        PlayActivity.activity.btNext.setVisibility(View.INVISIBLE);
    }

    private void initialization()
    {
        if (locaTionQuestion<20)
        {
            try{
                new getIMAGE(getActivity(),PlayActivity.arrayListAbbreviationsNameCountry.get(arrayListAnyQuestions.get(locaTionQuestion)),imgFlags);
                arrayList4Questions = PlayActivity.getRandomArrayList4(arrayListAnyQuestions.get(locaTionQuestion));
                Log.d("asdasd",arrayList4Questions.toString());
                for(int i =0; i < 4; i++)
                {
                    btAs[i].setText(PlayActivity.arrayListFullNameCountry.get(arrayList4Questions.get(i)));
                }
            }
            catch (Exception e)
            {
                Toast.makeText(getActivity(), "Ứng dụng cần kết nối Internet", Toast.LENGTH_SHORT).show();
                PlayActivity.handler.removeCallbacks(PlayActivity.r);
                HomeActivity.activity.getFlagsCountry();
                getActivity().finish();
            }
        }
        else {
            Toast.makeText(getActivity(), "20/20", Toast.LENGTH_SHORT).show();
        }

        handlerQuest = new Handler();
        rTrue = new Runnable() {
            @Override
            public void run() {
                if(time > 0) {
                    if (time % 2 == 1) {
                        btAs[loca].setBackgroundResource(R.drawable.buttonbordertrue);
                    } else {
                        btAs[loca].setBackgroundResource(R.drawable.buttonborder);
                    }
                    time--;
                    handlerQuest.postDelayed(rTrue,200);
                }
                else {
                    time = 5;
                    nextQuestion();
                    resetBackgroundButton();
                    handlerQuest.removeCallbacks(rTrue);
                }
            }
        };
        for(int i = 0; i< 4; i++)
        {
            if (arrayList4Questions.get(i).equals(arrayListAnyQuestions.get(locaTionQuestion))){
                True = i;
            }
        }
        rFalse = new Runnable() {
            @Override
            public void run() {
                if(time > 0) {
                    if (time % 2 == 1) {
                        btAs[True].setBackgroundResource(R.drawable.buttonbordertrue);
                    } else {
                        btAs[True].setBackgroundResource(R.drawable.buttonborder);
                    }
                    time--;
                    handlerQuest.postDelayed(rFalse,200);
                }
                else {
                    time = 5;
                    nextQuestion();
                    resetBackgroundButton();
                    handlerQuest.removeCallbacks(rFalse);
                }
            }
        };
    }

//    private void showDialog(final int loca)
//    {
//        dialogFragment = new DialogFragmentChosenAns(this,loca);
//        dialogFragment.show(getActivity().getSupportFragmentManager(),null);
//        Log.d("asdasd",""+dialogFragment.getId());
//    }

    private void changeBackgroundButton(int loca)
    {
        HomeActivity.startSoundBT(getContext());
        PlayActivity.activity.btNext.setVisibility(View.VISIBLE);
        resetBackgroundButton();
        btAs[loca].setBackgroundResource(R.drawable.buttonbordertrue);
        this.loca = loca;
    }

    private void resetBackgroundButton()
    {
        for(int i = 0; i < 4; i++)
        {
            btAs[i].setBackgroundResource(R.drawable.buttonborder);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btAnswer1:{
                changeBackgroundButton(0);
            }break;
            case R.id.btAnswer2:{
                changeBackgroundButton(1);
            }break;
            case R.id.btAnswer3:{
                changeBackgroundButton(2);
            }break;
            case R.id.btAnswer4:{
                changeBackgroundButton(3);
            }break;
            case R.id.btNext:{
                HomeActivity.startSoundBT(getContext());
                PlayActivity.activity.btNext.setVisibility(View.INVISIBLE);
                if(loca!=-1)
                {
                    finalQuestion();
                }
                else {
                    Toast.makeText(getActivity(), "Chưa chọn câu trả lời", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void finalQuestion()
    {
        PlayActivity.handler.removeCallbacks(PlayActivity.r);
        if(arrayList4Questions.get(loca).equals(arrayListAnyQuestions.get(locaTionQuestion))){
            if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
            {
                mediaTrue = MediaPlayer.create(getActivity(),R.raw.correctanswer);
                mediaTrue.start();
            }
            handlerQuest.postDelayed(rTrue,1000);
        }
        else {
            if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
            {
                mediaTrue = MediaPlayer.create(getActivity(),R.raw.wronganswer);
                mediaTrue.start();
            }
            btAs[loca].setBackgroundResource(R.drawable.buttonborderfalse);
            handlerQuest.postDelayed(rFalse,1000);
        }
    }

    private void nextQuestion()
    {
        if(arrayList4Questions.get(loca).equals(arrayListAnyQuestions.get(locaTionQuestion)))
        {
            Log.d("ans","as" + loca + 1);
            locaTionQuestion++;
            initialization();
            PlayActivity.time = 31;
            PlayActivity.handler.postDelayed(PlayActivity.r,PlayActivity.time);
            getBoolenQuestion.boolenquestion(true);
        }
        else {
            getBoolenQuestion.boolenquestion(false);
            PlayActivity.handler.removeCallbacks(PlayActivity.r);
        }
    }
}
