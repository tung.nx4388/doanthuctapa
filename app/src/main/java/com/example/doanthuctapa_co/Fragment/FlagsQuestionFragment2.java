package com.example.doanthuctapa_co.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.doanthuctapa_co.API.getIMAGE;
import com.example.doanthuctapa_co.Activity.HomeActivity;
import com.example.doanthuctapa_co.Activity.PlayActivity;
import com.example.doanthuctapa_co.DialogFragment.DialogFragmentChosenAns;
import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.R;

import java.util.ArrayList;

import static com.example.doanthuctapa_co.Activity.PlayActivity.arrayList4Questions;
import static com.example.doanthuctapa_co.Activity.PlayActivity.arrayListAnyQuestions;

public class FlagsQuestionFragment2 extends Fragment implements View.OnClickListener{

    MediaPlayer mediaTrue,mediaFalse;
    FlagsCountryItem flagsCountryItem;

    public interface getBoolenQuestion{
        void boolenquestion(boolean b);
    }

    FlagsQuestionFragment2.getBoolenQuestion getBoolenQuestion;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            getBoolenQuestion  = (FlagsQuestionFragment2.getBoolenQuestion) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    int locaTionQuestion;

    TextView txtNameFlag;
    ImageView[] imgFlag;
    ImageView[] imgBackground;
    ImageView[] imgBackgroundF;

    Handler handlerQuest;
    Runnable rTrue, rFalse;
    int time = 5;
    int loca;
    int True;

    DialogFragmentChosenAns dialogFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentflagsquestion2,container,false);

        flagsCountryItem = new FlagsCountryItem(getActivity());
        mapping(view);

        arrayListAnyQuestions = PlayActivity.randomArrayListAnyQuestion();
        initialization();

        Log.d("asdasd",PlayActivity.arrayListAbbreviationsNameCountry.toString());
        Log.d("asdasd",PlayActivity.arrayListFullNameCountry.toString());

        return view;
    }

    private void mapping(View v)
    {
        arrayListAnyQuestions = new ArrayList<>();
        arrayList4Questions = new ArrayList<>();
        locaTionQuestion = 0;
        loca = -1;

        txtNameFlag = v.findViewById(R.id.txtNameFlag);
        imgFlag = new ImageView[4];
        imgFlag[0] =v.findViewById(R.id.imgAnswer1);
        imgFlag[1] =v.findViewById(R.id.imgAnswer2);
        imgFlag[2] =v.findViewById(R.id.imgAnswer3);
        imgFlag[3] =v.findViewById(R.id.imgAnswer4);

        imgBackground = new ImageView[4];
        imgBackground[0] = v.findViewById(R.id.imgBackground1);
        imgBackground[1] = v.findViewById(R.id.imgBackground2);
        imgBackground[2] = v.findViewById(R.id.imgBackground3);
        imgBackground[3] = v.findViewById(R.id.imgBackground4);

        imgBackgroundF = new ImageView[4];
        imgBackgroundF[0] = v.findViewById(R.id.imgBackgroundF1);
        imgBackgroundF[1] = v.findViewById(R.id.imgBackgroundF2);
        imgBackgroundF[2] = v.findViewById(R.id.imgBackgroundF3);
        imgBackgroundF[3] = v.findViewById(R.id.imgBackgroundF4);

        imgFlag[0].setOnClickListener(this);
        imgFlag[1].setOnClickListener(this);
        imgFlag[2].setOnClickListener(this);
        imgFlag[3].setOnClickListener(this);
        PlayActivity.activity.btNext.setOnClickListener(this);
        PlayActivity.activity.btNext.setVisibility(View.GONE);
    }

    private void initialization()
    {
        if (locaTionQuestion<20)
        {
            try{
                txtNameFlag.setText(PlayActivity.arrayListFullNameCountry.get(arrayListAnyQuestions.get(locaTionQuestion)));
                arrayList4Questions = PlayActivity.getRandomArrayList4(arrayListAnyQuestions.get(locaTionQuestion));
                Log.d("asdasd",arrayList4Questions.toString());
                for(int i =0; i < 4; i++)
                {
                    new getIMAGE(getActivity(),PlayActivity.arrayListAbbreviationsNameCountry.get(arrayList4Questions.get(i)),imgFlag[i]);
                }
            }
            catch (Exception e)
            {
                Toast.makeText(getActivity(), "Ứng dụng cần kết nối Internet", Toast.LENGTH_SHORT).show();
                PlayActivity.handler.removeCallbacks(PlayActivity.r);
                HomeActivity.activity.getFlagsCountry();
                getActivity().finish();
            }
        }
        else {
            Toast.makeText(getActivity(), "20/20", Toast.LENGTH_SHORT).show();
        }

        handlerQuest = new Handler();
        rTrue = new Runnable() {
            @Override
            public void run() {
                if(time > 0) {
                    if (time % 2 == 1) {
                        imgBackground[loca].setVisibility(View.VISIBLE);
                    } else {
                        imgBackground[loca].setVisibility(View.GONE);
                    }
                    time--;
                    handlerQuest.postDelayed(rTrue,200);
                }
                else {
                    time = 5;
                    nextQuestion();
                    resetBackgroundButton();
                    handlerQuest.removeCallbacks(rTrue);
                }
            }
        };
        for(int i = 0; i< 4; i++)
        {
            if (arrayList4Questions.get(i).equals(arrayListAnyQuestions.get(locaTionQuestion))){
                True = i;
            }
        }
        rFalse = new Runnable() {
            @Override
            public void run() {
                if(time > 0) {
                    if (time % 2 == 1) {
                        imgBackground[True].setVisibility(View.VISIBLE);
                    } else {
                        imgBackground[True].setVisibility(View.GONE);
                    }
                    time--;
                    handlerQuest.postDelayed(rFalse,200);
                }
                else {
                    time = 5;
                    nextQuestion();
                    resetBackgroundButton();
                    handlerQuest.removeCallbacks(rFalse);
                }
            }
        };
    }

//    private void showDialog(final int loca)
//    {
//        dialogFragment = new DialogFragmentChosenAns(this,loca);
//        dialogFragment.show(getActivity().getSupportFragmentManager(),null);
//        Log.d("asdasd",""+dialogFragment.getId());
//    }

    private void changeBackgroundButton(int loca)
    {
        HomeActivity.startSoundBT(getContext());
        PlayActivity.activity.btNext.setVisibility(View.VISIBLE);
        resetBackgroundButton();
        imgBackground[loca].setVisibility(View.VISIBLE);
        this.loca = loca;
    }

    private void resetBackgroundButton()
    {
        for(int i = 0; i < 4; i++)
        {
            imgBackground[i].setVisibility(View.GONE);
            imgBackgroundF[i].setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.imgAnswer1:{
                changeBackgroundButton(0);
            }break;
            case R.id.imgAnswer2:{
                changeBackgroundButton(1);
            }break;
            case R.id.imgAnswer3:{
                changeBackgroundButton(2);
            }break;
            case R.id.imgAnswer4:{
                changeBackgroundButton(3);
            }break;
            case R.id.btNext:{
                HomeActivity.startSoundBT(getContext());
                PlayActivity.activity.btNext.setVisibility(View.GONE);
                if(loca!=-1)
                {
                    finalQuestion();
                }
                else {
                    Toast.makeText(getActivity(), "Chưa chọn câu trả lời", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void finalQuestion()
    {
        PlayActivity.handler.removeCallbacks(PlayActivity.r);
        if(arrayList4Questions.get(loca).equals(arrayListAnyQuestions.get(locaTionQuestion))){
            if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
            {
                mediaTrue = MediaPlayer.create(getActivity(),R.raw.correctanswer);
                mediaTrue.start();
            }
            handlerQuest.postDelayed(rTrue,1000);
        }
        else {
            if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
            {
                mediaTrue = MediaPlayer.create(getActivity(),R.raw.wronganswer);
                mediaTrue.start();
            }
            imgBackground[loca].setVisibility(View.GONE);
            imgBackgroundF[loca].setVisibility(View.VISIBLE);
            handlerQuest.postDelayed(rFalse,1000);
        }
    }

    public void nextQuestion()
    {
        if(arrayList4Questions.get(loca).equals(arrayListAnyQuestions.get(locaTionQuestion)))
        {
            Log.d("ans","as" + loca + 1);
            locaTionQuestion++;
            initialization();
            PlayActivity.time = 31;
            PlayActivity.handler.postDelayed(PlayActivity.r,PlayActivity.time);
            getBoolenQuestion.boolenquestion(true);
            loca = -1;
        }
        else {
            getBoolenQuestion.boolenquestion(false);
            PlayActivity.handler.removeCallbacks(PlayActivity.r);
        }
    }
}
