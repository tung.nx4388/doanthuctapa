package com.example.doanthuctapa_co.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

public class LogoAcitivity extends AppCompatActivity {

    FlagsCountryItem flagsCountryItem;
    boolean connected = false;
    ProgressBar progressBar;
    public static ArrayList<String> arrayListLanguage;

    Handler handler;
    Runnable runnable;

    int loading = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo_acitivity);

        progressBar = findViewById(R.id.progress_horizontal);

        progressBar.setProgress(0);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if(loading<100)
                {
                    loading++;
                    progressBar.setProgress(loading);
                    handler.postDelayed(runnable,20);
                }
                else {
                    handler.removeCallbacks(runnable);
                    TextView textView = findViewById(R.id.txtLoading);
                    textView.setText("");
                    YoYo.with(Techniques.TakingOff)
                            .duration(5000)
                            .repeat(0)
                            .onEnd(new YoYo.AnimatorCallback() {
                                @Override
                                public void call(Animator animator) {
                                    startActivity(new Intent(LogoAcitivity.this,HomeActivity.class));
                                    finish();
                                }
                            }).playOn(findViewById(R.id.fragmeLogo));
                }
            }
        };

        handler.postDelayed(runnable,20);

        arrayListLanguage = new ArrayList<>();

        creatFlags();

        getListLanguage();





        YoYo yoYo = null;
        yoYo.with(Techniques.Flash)
                .duration(10000)
                .repeat(0)
                .playOn(findViewById(R.id.txtLoading));
    }

    private void creatFlags()
    {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                //we are connected to a network
                connected = true;
            } else
                connected = false;
        } catch (Exception e) {

        }

        if (!connected) {
            Toast.makeText(this, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
        }

        flagsCountryItem = new FlagsCountryItem(LogoAcitivity.this);

        if (flagsCountryItem.getLANGUAGE().compareTo("") == 0) {
            try {
                getLanguage("vietnam");
            } catch (Exception e) {
                Toast.makeText(this, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (flagsCountryItem.getFlagsCountryItem().compareTo("") == 0) {
            try {
                getFlagsCountry();
            } catch (Exception e) {
                Toast.makeText(this, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if(flagsCountryItem.getSOUND().compareTo("") == 0)
        {
            flagsCountryItem.putSOUND("On");
        }
        progressBar.setProgress(66);
    }

    private void getLanguage(String language)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("languages").document(language);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("asdasd", "DocumentSnapshot data: " + document.getData());
                        flagsCountryItem.putLanguage(document.getData().toString());
                    } else {
                        Log.d("asdasd", "No such document");
                    }
                } else {
                    Log.d("asdasd", "get failed with ", task.getException());
                }
            }
        });
    }

    private void getFlagsCountry()
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("flagsquestion")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d("asd", document.getId() + " => " + document.getData());
                                flagsCountryItem.putFlagsCountryItem(document.getData().toString());
                            }
                        } else {
                            Log.w("asd", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void getListLanguage()
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("languages").document("selectlanguage");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("getLanguage", "DocumentSnapshot data: " + FlagsCountryItem.changeJSON(document.getData().toString()));
                        try {
                            JSONObject jsonObject = new JSONObject(FlagsCountryItem.changeJSON(document.getData().toString()));
                            JSONArray jsonArray = jsonObject.getJSONArray("listlanguage");
                            if(flagsCountryItem.getSELECTLANGUAGE().compareTo("") != 0)
                            {
                                arrayListLanguage.add(flagsCountryItem.getSELECTLANGUAGE().toUpperCase());
                            }
                            for(int i=0; i<jsonArray.length();i++)
                            {
                                if(flagsCountryItem.getSELECTLANGUAGE().compareTo(jsonArray.getString(i)) == 0)
                                {
                                    continue;
                                }
                                else {
                                    arrayListLanguage.add(jsonArray.getString(i).toUpperCase());
                                }
                            }
                            if(flagsCountryItem.getLANGUAGE().isEmpty())
                            {
                                getLanguage(arrayListLanguage.get(0).toLowerCase());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d("asdasd", "No such document");
                    }
                } else {
                    Log.d("asdasd", "get failed with ", task.getException());
                }
            }
        });
    }
}
