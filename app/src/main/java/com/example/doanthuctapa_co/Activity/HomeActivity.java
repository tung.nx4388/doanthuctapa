package com.example.doanthuctapa_co.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.doanthuctapa_co.API.getIMAGE;
import com.example.doanthuctapa_co.Adapter.AdapterViewPagerGuilde;
import com.example.doanthuctapa_co.Adapter.ItemResourceImage;
import com.example.doanthuctapa_co.DialogFragment.DialogFragmentWriteName;
import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, DialogFragmentWriteName.GetName {
    public static HomeActivity activity;
    public static String strMod;
    public static String name;

    TextView txtName;
    ImageView imgWorld;
    Button btStart,btSound,btHighscore,btMod,btInfor;
    Spinner spLanguage;
    public static MediaPlayer mediaHome,mediaBt;
    public static int isPlay;
    public static FlagsCountryItem flagsCountryItem;

    boolean connected = false;

    ArrayList<String> arrayListLanguage;

    DialogFragmentWriteName dialogFragmentWriteName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_home);

        activity = this;
        strMod = "Chế độ 1";


        creatFlags();
        mapping();
        if (LogoAcitivity.arrayListLanguage.size()>0)
        {
            arrayListLanguage = LogoAcitivity.arrayListLanguage;
            spinnerLanguage();
        }else {
            getListLanguage();
        }

        if (name.startsWith("#ID")) {
            dialogFragmentWriteName = new DialogFragmentWriteName();
            dialogFragmentWriteName.show(getSupportFragmentManager(), null);
        }

        showDialog();
    }

    private void showDialog()
    {
        if(flagsCountryItem.getRadioButton().compareTo("") == 0)
        {
            ArrayList<ItemResourceImage> itemResourceImages = new ArrayList<>();
            itemResourceImages.add(new ItemResourceImage(R.drawable.begin_vn_1,R.drawable.begin_en_1));
            itemResourceImages.add(new ItemResourceImage(R.drawable.mod_1_vn,R.drawable.mod_1_en));
            itemResourceImages.add(new ItemResourceImage(R.drawable.begin_vn_2,R.drawable.begin_en_2));
            itemResourceImages.add(new ItemResourceImage(R.drawable.mod_2_vn,R.drawable.mod_2_en));

            final Dialog dialog = new Dialog(HomeActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_fragment_guilde);
            dialog.setCanceledOnTouchOutside(false);

            AdapterViewPagerGuilde adapter = new AdapterViewPagerGuilde(HomeActivity.this,itemResourceImages);
            final ViewPager pager = dialog.findViewById(R.id.viewPager);
            DotsIndicator dots_indicator = dialog.findViewById(R.id.dots_indicator);
            RadioButton rdbt = dialog.findViewById(R.id.rdbt);
            dots_indicator.setViewPager(pager);
            pager.setAdapter(adapter);
            dialog.show();

            rdbt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b)
                    {
                        flagsCountryItem.putRadioButton("asdasd");
                        dialog.dismiss();
                    }
                }
            });
        }
    }

    private void creatFlags()
    {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                //we are connected to a network
                connected = true;
            } else
                connected = false;
        } catch (Exception e) {

        }

        if (!connected) {
            Toast.makeText(activity, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
        }

        flagsCountryItem = new FlagsCountryItem(HomeActivity.this);

        if (flagsCountryItem.getLANGUAGE().compareTo("") == 0) {
            try {
                getLanguage("vietnam");
            } catch (Exception e) {
                Toast.makeText(activity, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (flagsCountryItem.getFlagsCountryItem().compareTo("") == 0) {
            try {
                getFlagsCountry();
            } catch (Exception e) {
                Toast.makeText(activity, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if(flagsCountryItem.getSOUND().compareTo("") == 0)
        {
            flagsCountryItem.putSOUND("On");
        }
    }

    void mapping()
    {
        txtName = findViewById(R.id.txtName);
        imgWorld = findViewById(R.id.imgworld);
        try{
            new getIMAGE(this,"VN",imgWorld);
        }
        catch (Exception e)
        {
            Toast.makeText(activity, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
        }

        btStart = findViewById(R.id.btnTest);
        btHighscore = findViewById(R.id.btnHighScore);
        btSound = findViewById(R.id.btnSound);
        btMod = findViewById(R.id.btnMod);
        btInfor = findViewById(R.id.btnInformation);
        spLanguage = findViewById(R.id.spLanguage);

        btStart.setOnClickListener(this);
        btHighscore.setOnClickListener(this);
        btMod.setOnClickListener(this);
        btSound.setOnClickListener(this);
        txtName.setOnClickListener(this);
        btInfor.setOnClickListener(this);

        arrayListLanguage = new ArrayList<>();

        if(flagsCountryItem.getName().compareTo("")==0)
        {
            name = "#ID";
            Character[] characters = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
            for(int i=0; i<6;i++)
            {
                name = name + characters[new Random().nextInt(15)];
            }
        }
        else {
            name = flagsCountryItem.getName();
        }

        if(flagsCountryItem.getLANGUAGE().compareTo("") != 0)
        {
            txtName.setText(GetLanguage.getKey(this,"txthello")+" "+name);
            changeLanguage();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnMod:
            {
                startSoundBT(this);
                if(strMod.compareTo("Chế độ 1") == 0)
                {
                    strMod = "Chế độ 2";
                    btMod.setText(GetLanguage.getKey(this,"btmode2"));
                }
                else {
                    strMod = "Chế độ 1";
                    btMod.setText(GetLanguage.getKey(this,"btmode1"));
                }
            }break;
            case R.id.btnHighScore:
            {
                startSoundBT(this);
                if(isPlay == 1)
                {
                    isPlay = 2;
                }
                startActivity(new Intent(this,HighscoreActivity.class));
            }break;
            case R.id.btnTest:
            {
                startSoundBT(this);
                if(isPlay == 1)
                {
                    isPlay = 2;
                }
                startActivity(new Intent(this,ChosenLeveActivity.class));
            }break;
            case R.id.btnSound:
            {
                startSoundBT(this);
                if(flagsCountryItem.getSOUND().compareTo("On") != 0)
                {
                    btSound.setText(GetLanguage.getKey(this,"btmusicon"));
                    mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                    mediaHome.start();
                    flagsCountryItem.putSOUND("On");
                }
                else {
                    btSound.setText(GetLanguage.getKey(this, "btmusicoff"));
                    mediaHome.stop();
                    flagsCountryItem.putSOUND("Off");
                }
            }break;
            case R.id.txtName:
            {
                startSoundBT(this);
                dialogFragmentWriteName = new DialogFragmentWriteName();
                dialogFragmentWriteName.show(getSupportFragmentManager(),null);
            }break;
            case R.id.btnInformation:{
                startSoundBT(this);
                if(isPlay == 1)
                {
                    isPlay = 2;
                }
                startActivity(new Intent(this,InfoAcitivity.class));
            }break;
        }
    }
    public void getFlagsCountry()
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("flagsquestion")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d("asd", document.getId() + " => " + document.getData());
                                flagsCountryItem.putFlagsCountryItem(document.getData().toString());
                            }
                        } else {
                            Log.w("asd", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void getLanguage(String language)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("languages").document(language);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("asdasd", "DocumentSnapshot data: " + document.getData());
                        flagsCountryItem.putLanguage(document.getData().toString());
                        changeLanguage();
                    } else {
                        Log.d("asdasd", "No such document");
                    }
                } else {
                    Log.d("asdasd", "get failed with ", task.getException());
                }
            }
        });
    }

    private void getListLanguage()
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("languages").document("selectlanguage");
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("getLanguage", "DocumentSnapshot data: " + FlagsCountryItem.changeJSON(document.getData().toString()));
                        try {
                            JSONObject jsonObject = new JSONObject(FlagsCountryItem.changeJSON(document.getData().toString()));
                            JSONArray jsonArray = jsonObject.getJSONArray("listlanguage");
                            if(flagsCountryItem.getSELECTLANGUAGE().compareTo("") != 0)
                            {
                                arrayListLanguage.add(flagsCountryItem.getSELECTLANGUAGE().toUpperCase());
                            }
                            for(int i=0; i<jsonArray.length();i++)
                            {
                                if(flagsCountryItem.getSELECTLANGUAGE().compareTo(jsonArray.getString(i)) == 0)
                                {
                                    continue;
                                }
                                else {
                                    arrayListLanguage.add(jsonArray.getString(i).toUpperCase());
                                }
                            }
                            spinnerLanguage();
                            if(flagsCountryItem.getLANGUAGE().isEmpty())
                            {
                                getLanguage(arrayListLanguage.get(0).toLowerCase());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d("asdasd", "No such document");
                    }
                } else {
                    Log.d("asdasd", "get failed with ", task.getException());
                }
            }
        });
    }

    @Override
    public void name(String name) {
        flagsCountryItem.putName(name);
        HomeActivity.name = name;
        if (flagsCountryItem.getLANGUAGE().compareTo("") != 0)
        {
            txtName.setText(GetLanguage.getKey(HomeActivity.this,"txthello")+" "+name);
        }
    }

    private void spinnerLanguage()
    {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.custum_simple_spinner_item, arrayListLanguage);
        dataAdapter.setDropDownViewResource(R.layout.custum_simple_spinner_item);
        spLanguage.setAdapter(dataAdapter);
        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Log.d("asdasd",item);
                if(!flagsCountryItem.getLANGUAGE().isEmpty())
                {
                    if(GetLanguage.getKey(HomeActivity.this,"language").compareTo(item.toLowerCase()) != 0)
                    {
                        flagsCountryItem.putSELECTLANGUAGE(item.toLowerCase());
                        getLanguage(flagsCountryItem.getSELECTLANGUAGE());
                        getvaluelanguage(item);
                    }
                    else {
                        changeLanguage();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void getvaluelanguage(String language)
    {
        if(arrayListLanguage.size() > 0)
        {
            if(arrayListLanguage.get(0).compareTo(language) != 0)
            {
                arrayListLanguage.add(arrayListLanguage.get(0));
                arrayListLanguage.remove(0);
                spinnerLanguage();
            }
        }
    }

    private void changeLanguage()
    {
        txtName.setText(GetLanguage.getKey(this,"txthello")+" "+name);
        btStart.setText(GetLanguage.getKey(this,"btplay"));
        btHighscore.setText(GetLanguage.getKey(this,"btrecord"));
        if(flagsCountryItem.getSOUND().compareTo("On") == 0)
        {
            btSound.setText(GetLanguage.getKey(this,"btmusicon"));
        }
        else {
            btSound.setText(GetLanguage.getKey(this,"btmusicoff"));
        }
        btInfor.setText(GetLanguage.getKey(this,"btmoreinfo"));
        if(strMod.compareTo("Chế độ 1") == 0)
        {
            btMod.setText(GetLanguage.getKey(this,"btmode1"));
        }
        else {
            btMod.setText(GetLanguage.getKey(this,"btmode2"));
        }
    }

    public static void startSoundBT(Context context)
    {
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0) {
            mediaBt = MediaPlayer.create(context,R.raw.preview);
            mediaBt.start();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(isPlay == 0)
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
                isPlay = 1;
            }
        }
        Log.d("Tung","Start");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(mediaHome.isPlaying())
            {
                mediaHome.stop();
            }

        }
        Log.d("Tung","Destroy");

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(mediaHome.isPlaying())
            {
                if(isPlay == 1)
                {
                    mediaHome.stop();
                }
                else {
                    isPlay = 1;
                }
            }

        }
        Log.d("Tung","Stop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(!mediaHome.isPlaying())
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Resume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0) {
            if (!mediaHome.isPlaying()) {
                mediaHome = MediaPlayer.create(this, R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Restart");
    }
}
