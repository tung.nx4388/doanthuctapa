package com.example.doanthuctapa_co.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doanthuctapa_co.Adapter.AdapterRecycleListHighScore;
import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.Highscore.HighscoreItem;
import com.example.doanthuctapa_co.Interface.SetDataHighscore;
import com.example.doanthuctapa_co.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Objects;

import static com.example.doanthuctapa_co.Activity.HomeActivity.flagsCountryItem;
import static com.example.doanthuctapa_co.Activity.HomeActivity.isPlay;
import static com.example.doanthuctapa_co.Activity.HomeActivity.mediaHome;

public class HighscoreActivity extends AppCompatActivity implements View.OnClickListener, SetDataHighscore {

    SetDataHighscore setDataHighscore;

    TextView txtRank,txtName,txtScore;
    TextView txtEasy,txtNormal,txtHard;
    TextView txtMod1, txtMod2;
    RecyclerView recyclerView;

    TextView txtHighscore;

    ArrayList<HighscoreItem>[] easyList,normalList,hardList;
    AdapterRecycleListHighScore[] easyAdapter,normalAdapter,hardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        mapping();
        initialization();
        try{
            readData();
        }
        catch (Exception e)
        {
            Toast.makeText(this, "App chưa kết nối Internet", Toast.LENGTH_SHORT).show();
        }
        changeLanguage();
    }

    private void mapping()
    {
        txtEasy = findViewById(R.id.txtEasy);
        txtNormal = findViewById(R.id.txtNormal);
        txtHard = findViewById(R.id.txtHard);
        recyclerView = findViewById(R.id.recylerView);
        txtMod1 = findViewById(R.id.txtMod1);
        txtMod2 = findViewById(R.id.txtMod2);
        txtRank = findViewById(R.id.txtRank);
        txtName = findViewById(R.id.txtName);
        txtScore = findViewById(R.id.txtScore);
        txtHighscore = findViewById(R.id.txtHighScore);
    }

    private void initialization()
    {
        setDataHighscore = this;

        txtEasy.setOnClickListener(this);
        txtNormal.setOnClickListener(this);
        txtHard.setOnClickListener(this);
        txtMod1.setOnClickListener(this);
        txtMod2.setOnClickListener(this);

        easyList = new ArrayList[2];
        normalList = new ArrayList[2];
        hardList = new ArrayList[2];

        easyList[0] = new ArrayList<>();
        easyList[1] = new ArrayList<>();
        normalList[0] = new ArrayList<>();
        normalList[1] = new ArrayList<>();
        hardList[0] = new ArrayList<>();
        hardList[1] = new ArrayList<>();

        easyAdapter = new AdapterRecycleListHighScore[2];
        normalAdapter = new AdapterRecycleListHighScore[2];
        hardAdapter = new AdapterRecycleListHighScore[2];
    }

    private void setBackgroundENH(int i)
    {
        HomeActivity.startSoundBT(this);
        txtEasy.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        txtNormal.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        txtHard.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        switch (i)
        {
            case 1:{
                txtEasy.setBackgroundColor(Color.parseColor("#37000000"));
            }break;
            case 2:{
                txtNormal.setBackgroundColor(Color.parseColor("#37000000"));
            }break;
            case 3:{
                txtHard.setBackgroundColor(Color.parseColor("#37000000"));
            }break;
        }
    }

    private void setBackgroundMod(int i)
    {
        HomeActivity.startSoundBT(this);
        txtMod1.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        txtMod2.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        switch (i)
        {
            case 1:{
                txtMod1.setBackgroundColor(Color.parseColor("#37000000"));
            }break;
            case 2:{
                txtMod2.setBackgroundColor(Color.parseColor("#37000000"));
            }break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txtEasy:
            {
                setBackgroundENH(1);
                setDataHighscore.setEND(0);
            }break;
            case R.id.txtNormal:
            {
                setBackgroundENH(2);
                setDataHighscore.setEND(1);
            }break;
            case R.id.txtHard:
            {
                setBackgroundENH(3);
                setDataHighscore.setEND(2);
            }break;
            case R.id.txtMod1:{
                setBackgroundMod(1);
                setDataHighscore.setMod(0);
            }break;
            case R.id.txtMod2:{
                setBackgroundMod(2);
                setDataHighscore.setMod(1);
            }break;
        }
    }
    private void readData()
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("highscore")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                switch (document.getId())
                                {
                                    case "easy":
                                    {
                                        easyList[0] = HighscoreItem.getArrayListHighscore(FlagsCountryItem.changeJSON(document.getData().toString()).replace('=',':'));
                                        easyAdapter[0] = new AdapterRecycleListHighScore(easyList[0]);
                                        recyclerView.setAdapter(easyAdapter[0]);
                                    }break;
                                    case "normal":
                                    {
                                        normalList[0] = HighscoreItem.getArrayListHighscore(FlagsCountryItem.changeJSON(document.getData().toString()).replace('=',':'));
                                        normalAdapter[0] = new AdapterRecycleListHighScore(normalList[0]);
                                    }break;
                                    case "hard":
                                    {
                                        hardList[0] = HighscoreItem.getArrayListHighscore(FlagsCountryItem.changeJSON(document.getData().toString()).replace('=',':'));
                                        hardAdapter[0] = new AdapterRecycleListHighScore(hardList[0]);
                                    }break;
                                    case "easymod2":
                                    {
                                        easyList[1] = HighscoreItem.getArrayListHighscore(FlagsCountryItem.changeJSON(document.getData().toString()).replace('=',':'));
                                        easyAdapter[1] = new AdapterRecycleListHighScore(easyList[1]);
                                    }break;
                                    case "normalmod2":
                                    {
                                        normalList[1] = HighscoreItem.getArrayListHighscore(FlagsCountryItem.changeJSON(document.getData().toString()).replace('=',':'));
                                        normalAdapter[1] = new AdapterRecycleListHighScore(normalList[1]);
                                    }break;
                                    case "hardmod2":
                                    {
                                        hardList[1] = HighscoreItem.getArrayListHighscore(FlagsCountryItem.changeJSON(document.getData().toString()).replace('=',':'));
                                        hardAdapter[1] = new AdapterRecycleListHighScore(hardList[1]);
                                    }break;
                                }
                            }
                        } else {
                            Log.w("asd", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    int end,mod;

    @Override
    public void setEND(int end) {
        this.end = end;
        switch (end)
        {
            case 0:{
                recyclerView.setAdapter(easyAdapter[mod]);
            }break;
            case 1:{
                recyclerView.setAdapter(normalAdapter[mod]);
            }break;
            case 2:{
                recyclerView.setAdapter(hardAdapter[mod]);
            }break;
        }

    }

    @Override
    public void setMod(int mod) {
        this.mod = mod;
        switch (end)
        {
            case 0:{
                recyclerView.setAdapter(easyAdapter[mod]);
            }break;
            case 1:{
                recyclerView.setAdapter(normalAdapter[mod]);
            }break;
            case 2:{
                recyclerView.setAdapter(hardAdapter[mod]);
            }break;
        }
    }

    private void changeLanguage()
    {
        txtEasy.setText(GetLanguage.getKey(this,"bteasy"));
        txtNormal.setText(GetLanguage.getKey(this,"btmedium"));
        txtHard.setText(GetLanguage.getKey(this,"bthard"));
        txtMod1.setText(GetLanguage.getKey(this,"btmode1"));
        txtMod2.setText(GetLanguage.getKey(this,"btmode2"));
        txtName.setText(GetLanguage.getKey(this,"txtname"));
        txtScore.setText(GetLanguage.getKey(this,"txtscore"));
        txtHighscore.setText(GetLanguage.getKey(this,"btrecord"));
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(isPlay == 0)
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.start();
                mediaHome.setLooping(true);
                isPlay = 1;
            }
        }
        Log.d("Tung","Start");
    }

    @Override
    public void onBackPressed() {
        isPlay = 2;
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(mediaHome.isPlaying())
            {
                if(isPlay == 1)
                {
                    mediaHome.stop();
                }
                else {
                    isPlay = 1;
                }
            }

        }
        Log.d("Tung","ChoseStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(!mediaHome.isPlaying())
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Resume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0) {
            if (!mediaHome.isPlaying()) {
                mediaHome = MediaPlayer.create(this, R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Restart");
    }
}
