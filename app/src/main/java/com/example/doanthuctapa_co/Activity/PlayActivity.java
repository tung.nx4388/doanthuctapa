package com.example.doanthuctapa_co.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.doanthuctapa_co.DialogFragment.DialogFragmentFinish;
import com.example.doanthuctapa_co.DialogFragment.DialogFragmentRank;
import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.FlagsCountry.GetFlagsCountry;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.Fragment.FlagsQuestionFragment1;
import com.example.doanthuctapa_co.Fragment.FlagsQuestionFragment2;
import com.example.doanthuctapa_co.Highscore.HighscoreItem;
import com.example.doanthuctapa_co.Highscore.InsertNewHighScore;
import com.example.doanthuctapa_co.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Random;

public class PlayActivity extends AppCompatActivity implements FlagsQuestionFragment1.getBoolenQuestion,DialogFragmentFinish.getFinal,FlagsQuestionFragment2.getBoolenQuestion,DialogFragmentRank.getRank {
    public static PlayActivity activity;
    MediaPlayer mediaPlay;
    int isPlay;
    FlagsCountryItem flagsCountryItem;

    DialogFragmentFinish dialogFragmentFinish;
    ArrayList<HighscoreItem> highscoreItemArrayList;
    ArrayList<HighscoreItem> arrayList;

    TextView txtMod, txtCountTime, txtCountQuestions;
    Intent intent;
    public static String chosenLeve;
    public static Handler handler;
    public static Runnable r,r1;
    public static int time;
    public static int locationQuestions;
    public static String lv;

    private String mod;

    public Button btNext;

    public static ArrayList<String> arrayListAbbreviationsNameCountry, arrayListFullNameCountry;
    //20 cau, 4 cau tra loi moi cau
    public static ArrayList<Integer> arrayListAnyQuestions;
    public static ArrayList<Integer> arrayList4Questions;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        activity = this;

        mapping();
        intent = getIntent();
        chosenLeve = intent.getStringExtra("chosenLeve");
        flagsCountryItem = new FlagsCountryItem(this);

        initialization();

        switch (chosenLeve) {
            case "Easy": {
                arrayListAbbreviationsNameCountry = GetFlagsCountry.getEasyAbbreviationsNameCountry(this);
//                arrayListFullNameCountry = GetFlagsCountry.getEasyFullNameCountry(this);
            }
            break;
            case "Normal": {
                arrayListAbbreviationsNameCountry = GetFlagsCountry.getNormAlabbreviationsNameCountry(this);
//                arrayListFullNameCountry = GetFlagsCountry.getNormalFullNameCountry(this);
            }
            break;
            case "Hard": {
                arrayListAbbreviationsNameCountry = GetFlagsCountry.getHardAbbreviationsNameCountry(this);
//                arrayListFullNameCountry = GetFlagsCountry.getHardFullNameCountry(this);
            }
            break;
        }

        for(int i=0;i<arrayListAbbreviationsNameCountry.size();i++)
        {
            arrayListFullNameCountry.add(GetLanguage.getKey(this,"txt"+arrayListAbbreviationsNameCountry.get(i).toLowerCase()));
        }

        if (HomeActivity.strMod.compareTo("Chế độ 1") == 0) {
            FlagsQuestionFragment1 flagsQuestionFragment1 = new FlagsQuestionFragment1();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, flagsQuestionFragment1).commit();
            mod = chosenLeve.toLowerCase();
        }
        else {
            FlagsQuestionFragment2 flagsQuestionFragment2 = new FlagsQuestionFragment2();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, flagsQuestionFragment2).commit();
            mod = chosenLeve.toLowerCase() + "mod2";
        }

        readFirebaseData();

    }

    void mapping()
    {
        highscoreItemArrayList = new ArrayList<>();

        chosenLeve = "";
        arrayListAbbreviationsNameCountry = new ArrayList<>();
        arrayListFullNameCountry = new ArrayList<>();

        txtMod = findViewById(R.id.txtMod);
        txtCountQuestions = findViewById(R.id.txtCountQuestion);
        txtCountTime = findViewById(R.id.txtTime);

        btNext = findViewById(R.id.btNext);
    }

    void initialization()
    {
        lv = "";
        switch (chosenLeve){
            case "Easy":{
                lv = "Dễ";
            }break;
            case "Normal":{
                lv = "Trung bình";
            }break;
            case "Hard":{
                lv = "Khó";
            }break;
        }
        if(GetLanguage.getKey(this,"language").compareTo("vietnam") == 0)
        {
            txtMod.setText("Chế độ: "+lv);
        }
        else {
            txtMod.setText(chosenLeve + " Mode");
        }
        getTime();
        locationQuestions = 0;
        txtCountQuestions.setText(+locationQuestions+1+"/20");
    }

    public static ArrayList<Integer> randomArrayListAnyQuestion()
    {
        ArrayList<Integer> arrayList = new ArrayList<>();
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        for(int i = 0; i<20 ; i++)
        {
            arrayList1.add(i);
        }
        for(int i =0; i<20; i++)
        {
            int stt = new Random().nextInt(arrayList1.size());
            arrayList.add(arrayList1.get(stt));
            arrayList1.remove(stt);
        }
        arrayList1.clear();
        return arrayList;
    }
    public static ArrayList<Integer> getRandomArrayList4(int loca)
    {
        ArrayList<Integer> arrayList = new ArrayList<>();
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.addAll(arrayListAnyQuestions);
        arrayList.add(loca);
        while (arrayList.size() <4) {
            int stt = new Random().nextInt(arrayList1.size());
            if(arrayList1.get(stt) != loca)
            {
                arrayList.add(arrayList1.get(stt));
                arrayList1.remove(stt);
            }
        }
        ArrayList<Integer> arrayList2 = new ArrayList<>();
        for(int i =0; i < 4;i++)
        {
            int stt = new Random().nextInt(arrayList.size());
            arrayList2.add(arrayList.get(stt));
            arrayList.remove(stt);
        }
        arrayList.clear();
        arrayList1.clear();
        return arrayList2;
    }

    void getTime()
    {
        time = 31;
        String strTime = "";
        if(GetLanguage.getKey(this,"language").compareTo("vietnam") == 0)
        {
            strTime = "Thời gian";
        }
        else {
            strTime = "Time";
        }
        handler = new Handler();
        final String finalStrTime = strTime;
        r = new Runnable() {
            @Override
            public void run() {
                if(time >= 1)
                {
                    time --;
                    txtCountTime.setText(finalStrTime +": "+time);
                    handler.postDelayed(r,1000);
                }
                else {
                    handler.removeCallbacks(r);
                    dialogFragmentFinish = new DialogFragmentFinish(PlayActivity.locationQuestions,PlayActivity.lv);
                    dialogFragmentFinish.show(getSupportFragmentManager(),null);
                }
                if(time <=3 && time >= 0)
                {
                    r1 = new Runnable() {
                        @Override
                        public void run() {
                            txtCountTime.setTextColor(Color.parseColor("#FFFFFF"));
                        }
                    };
                    handler.postDelayed(r1,500);
                    txtCountTime.setTextColor(Color.parseColor("#FF0000"));
                }
                else {
                    txtCountTime.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }
        };
        handler.postDelayed(r,1000);
    }

    @Override
    public void boolenquestion(boolean b) {
        if(b)
        {
            locationQuestions++;
            if(locationQuestions <= 19)
            {
                txtCountQuestions.setText(+locationQuestions+1+"/20");
            }
            else {
                dialogFragmentFinish = new DialogFragmentFinish(PlayActivity.locationQuestions,PlayActivity.lv);
                dialogFragmentFinish.show(getSupportFragmentManager(),null);
                handler.removeCallbacks(r);
            }
        }
        else {
            dialogFragmentFinish = new DialogFragmentFinish(PlayActivity.locationQuestions,PlayActivity.lv);
            dialogFragmentFinish.show(getSupportFragmentManager(),null);
            handler.removeCallbacks(r);
        }
    }

    @Override
    public void finall(boolean b) {
        if(b)
        {
            if (highscoreItemArrayList.size() < 20 )
            {
                insertFirebaseData(locationQuestions,HomeActivity.name);
                HighscoreItem highscoreItem = InsertNewHighScore.findRank(arrayList,HomeActivity.name,""+locationQuestions);
                DialogFragmentRank dialogFragmentRank = new DialogFragmentRank(highscoreItem.getName(),highscoreItem.getRank(),highscoreItem.getScore(),mod);
                dialogFragmentRank.show(getSupportFragmentManager(),null);
            }
            else {
                if (locationQuestions >= Integer.parseInt(highscoreItemArrayList.get(highscoreItemArrayList.size() - 1).getScore())) {
                    insertFirebaseData(locationQuestions,HomeActivity.name);
                    HighscoreItem highscoreItem = InsertNewHighScore.findRank(arrayList,HomeActivity.name,""+locationQuestions);
                    DialogFragmentRank dialogFragmentRank = new DialogFragmentRank(highscoreItem.getName(),highscoreItem.getRank(),highscoreItem.getScore(),mod);
                    dialogFragmentRank.show(getSupportFragmentManager(),null);
                } else {
                    finish();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        dialogFragmentFinish = new DialogFragmentFinish(PlayActivity.locationQuestions,PlayActivity.lv);
        dialogFragmentFinish.show(getSupportFragmentManager(),null);
        handler.removeCallbacks(r);
    }

    private void readFirebaseData()
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("highscore").document(mod);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("asdasd", "DocumentSnapshot data: " + document.getData());
                        highscoreItemArrayList = HighscoreItem.getArrayListHighscore(FlagsCountryItem.changeJSON(document.getData().toString()).replace('=',':'));
                    } else {
                        Log.d("asdasd", "No such document");
                    }
                } else {
                    Log.d("asdasd", "get failed with ", task.getException());
                }
            }
        });
    }

    private void insertFirebaseData(int score, String name)
    {
        highscoreItemArrayList.add(new HighscoreItem("",name,""+score));
        arrayList = InsertNewHighScore.sortHighScoreItemArrayList(highscoreItemArrayList);
        ArrayList<String> arrayListName = new ArrayList<>();
        ArrayList<String> arrayListScore = new ArrayList<>();
        ArrayList<String> arrayListRank = new ArrayList<>();

        for(int i = 0; i < arrayList.size();i++)
        {
            arrayListName.add(arrayList.get(i).getName());
            arrayListRank.add(arrayList.get(i).getRank());
            arrayListScore.add(arrayList.get(i).getScore());
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("highscore").document(mod)
                .update(
                  "name",arrayListName,
                        "score",arrayListScore,
                        "rank",arrayListRank
                );
    }

    @Override
    public void ranK(boolean b) {
        if(b)
        {
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(isPlay == 0)
            {
                mediaPlay = MediaPlayer.create(this,R.raw.soundplay);
                mediaPlay.setLooping(true);
                mediaPlay.start();
                isPlay = 1;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(mediaPlay.isPlaying())
            {
                mediaPlay.stop();
            }

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(mediaPlay.isPlaying())
            {
                mediaPlay.stop();
            }

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(!mediaPlay.isPlaying())
            {
                mediaPlay = MediaPlayer.create(this,R.raw.soundplay);
                mediaPlay.setLooping(true);
                mediaPlay.start();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(!mediaPlay.isPlaying())
            {
                mediaPlay = MediaPlayer.create(this,R.raw.soundplay);
                mediaPlay.setLooping(true);
                mediaPlay.start();
            }
        }
    }
}
