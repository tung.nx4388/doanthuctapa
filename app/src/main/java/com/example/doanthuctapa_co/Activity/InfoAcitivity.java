package com.example.doanthuctapa_co.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.R;

import static com.example.doanthuctapa_co.Activity.HomeActivity.flagsCountryItem;
import static com.example.doanthuctapa_co.Activity.HomeActivity.isPlay;
import static com.example.doanthuctapa_co.Activity.HomeActivity.mediaHome;

public class InfoAcitivity extends AppCompatActivity {

    FlagsCountryItem flagsCountryItem;

    TextView txtPeo,txtThank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_acitivity);

        flagsCountryItem = new FlagsCountryItem(this);
        mapping();
        changLanguage();

    }
    private void mapping(){
        txtPeo = findViewById(R.id.txtPeople);
        txtThank = findViewById(R.id.txtThankYou);
    }
    private void changLanguage(){
        if(GetLanguage.getKey(this,"language").compareTo("vietnam") != 0)
        {
            txtPeo.setText("Presented by:");
            txtThank.setText("Thanks for your support!");
        }
        else {
            txtPeo.setText("Người thực hiện:");
            txtThank.setText("Cám ơn các bạn đã ủng hộ chúng tôi!");
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(isPlay == 0)
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.start();
                mediaHome.setLooping(true);
                isPlay = 1;
            }
        }
        Log.d("Tung","Start");
    }

    @Override
    public void onBackPressed() {
        isPlay = 2;
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(mediaHome.isPlaying())
            {
                if(isPlay == 1)
                {
                    mediaHome.stop();
                }
                else {
                    isPlay = 1;
                }
            }

        }
        Log.d("Tung","ChoseStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(!mediaHome.isPlaying())
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Resume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0) {
            if (!mediaHome.isPlaying()) {
                mediaHome = MediaPlayer.create(this, R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Restart");
    }
}
