package com.example.doanthuctapa_co.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.doanthuctapa_co.FlagsCountry.FlagsCountryItem;
import com.example.doanthuctapa_co.FlagsCountry.GetLanguage;
import com.example.doanthuctapa_co.R;

import static com.example.doanthuctapa_co.Activity.HomeActivity.flagsCountryItem;
import static com.example.doanthuctapa_co.Activity.HomeActivity.isPlay;
import static com.example.doanthuctapa_co.Activity.HomeActivity.mediaHome;

public class ChosenLeveActivity extends AppCompatActivity implements View.OnClickListener {

    Button btEz,btNormal,btHard;
    TextView txtChosenLv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chosen_leve);

        mapping();
        changeLanguage();

    }

    void mapping()
    {
        btEz = findViewById(R.id.btnEz);
        btHard = findViewById(R.id.btnHard);
        btNormal = findViewById(R.id.btnNorm);
        txtChosenLv = findViewById(R.id.txtChosenLv);

        btEz.setOnClickListener(this);
        btNormal.setOnClickListener(this);
        btHard.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnEz:{
                HomeActivity.startSoundBT(this);
                finish();
                Intent intent = new Intent(this,PlayActivity.class);
                intent.putExtra("chosenLeve","Easy");
                startActivity(intent);
            }break;
            case R.id.btnNorm:{
                HomeActivity.startSoundBT(this);
                finish();
                Intent intent = new Intent(this,PlayActivity.class);
                intent.putExtra("chosenLeve","Normal");
                startActivity(intent);
            }break;
            case R.id.btnHard:{
                HomeActivity.startSoundBT(this);
                finish();
                Intent intent = new Intent(this,PlayActivity.class);
                intent.putExtra("chosenLeve","Hard");
                startActivity(intent);
            }break;
        }
    }

    private void changeLanguage()
    {
        txtChosenLv.setText(GetLanguage.getKey(this,"txtchoosemode"));
        btEz.setText(GetLanguage.getKey(this,"bteasy"));
        btNormal.setText(GetLanguage.getKey(this,"btmedium"));
        btHard.setText(GetLanguage.getKey(this,"bthard"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(isPlay == 0)
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
                isPlay = 1;
            }
        }
        Log.d("Tung","Start");
    }

    @Override
    public void onBackPressed() {
        isPlay = 2;
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(mediaHome.isPlaying())
            {
                if(isPlay == 1)
                {
                    mediaHome.stop();
                }
                else {
                    isPlay = 1;
                }
            }

        }
        Log.d("Tung","ChoseStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(flagsCountryItem.getSOUND().compareTo("Off") != 0)
        {
            if(!mediaHome.isPlaying())
            {
                mediaHome = MediaPlayer.create(this,R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Resume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (flagsCountryItem.getSOUND().compareTo("Off") != 0) {
            if (!mediaHome.isPlaying()) {
                mediaHome = MediaPlayer.create(this, R.raw.soundhome);
                mediaHome.setLooping(true);
                mediaHome.start();
            }
        }
        Log.d("Tung","Restart");
    }
}
